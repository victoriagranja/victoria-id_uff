class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.float :final
      t.float :p1
      t.float :p2
      t.integer :status

      t.timestamps
    end
  end
end
