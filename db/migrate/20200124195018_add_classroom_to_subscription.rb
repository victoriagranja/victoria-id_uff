class AddClassroomToSubscription < ActiveRecord::Migration[5.2]
  def change
    add_reference :subscriptions, :classroom, foreign_key: true
  end
end
