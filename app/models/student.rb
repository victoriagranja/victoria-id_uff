class Student < ApplicationRecord
    belongs_to :user
    has_many :subscripitions
    has_many :classrooms, through: :subscripition
end
