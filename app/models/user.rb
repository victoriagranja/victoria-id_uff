class User < ApplicationRecord
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
  validates :email, :format => { :with => /id.uff.br\Z/i, :on => :create }
  validates :password, length: { minimum: 6 }

  has_one :teacher
  has_one :student

  enum kind: [
    :admin,
    :teacher,
    :student,
    :secretary
  ]

  devise :database_authenticatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: JwtBlacklist
end