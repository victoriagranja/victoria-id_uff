class Subject < ApplicationRecord
    has_many :requirements
    has_many :disciplines, through: :requirements, class_name: "Subject"

    has_many :licenses
    has_many :teachers, through: :license
end
