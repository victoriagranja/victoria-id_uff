class Classroom < ApplicationRecord
  belongs_to :teacher
  enum status:[
    "pendant",
    "active"
  ]
end
